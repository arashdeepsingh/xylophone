/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xolo;

import java.applet.AudioClip;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * @author arashdeep
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Label label;
    final javafx.scene.media.AudioClip bar1Note;
    final javafx.scene.media.AudioClip bar2Note;
    final javafx.scene.media.AudioClip bar3Note;
    final javafx.scene.media.AudioClip bar4Note;
    final javafx.scene.media.AudioClip bar5Note;
    final javafx.scene.media.AudioClip bar6Note;
    final javafx.scene.media.AudioClip bar7Note;
    final javafx.scene.media.AudioClip bar8Note;

    public FXMLDocumentController() {

        bar1Note
                = new javafx.scene.media.AudioClip(FXMLDocumentController.class.getResource("Note1.wav").toString());
        bar2Note
                = new javafx.scene.media.AudioClip(FXMLDocumentController.class.getResource("Note2.wav").toString());
        bar3Note
                = new javafx.scene.media.AudioClip(FXMLDocumentController.class.getResource("Note3.wav").toString());
        bar4Note
                = new javafx.scene.media.AudioClip(FXMLDocumentController.class.getResource("Note4.wav").toString());
        bar5Note
                = new javafx.scene.media.AudioClip(FXMLDocumentController.class.getResource("Note5.wav").toString());
        bar6Note
                = new javafx.scene.media.AudioClip(FXMLDocumentController.class.getResource("Note6.wav").toString());
        bar7Note
                = new javafx.scene.media.AudioClip(FXMLDocumentController.class.getResource("Note7.wav").toString());
        bar8Note
                = new javafx.scene.media.AudioClip(FXMLDocumentController.class.getResource("Note8.wav").toString());

    }

    @FXML
    private void actionone(ActionEvent event) throws LineUnavailableException, UnsupportedAudioFileException, IOException {
        bar1Note.play();
    }

    @FXML
    private void actiontwo(ActionEvent event) throws LineUnavailableException, UnsupportedAudioFileException {
        bar2Note.play();
    }

    @FXML
    private void actionthree(ActionEvent event) {
        bar3Note.play();
    }

    @FXML
    private void actionfour(ActionEvent event) {
        bar4Note.play();
    }

    @FXML
    private void actionfive(ActionEvent event) {
        bar5Note.play();
    }

    @FXML
    private void actionsix(ActionEvent event) {
        bar6Note.play();
    }

    @FXML
    private void actionseven(ActionEvent event) {
        bar7Note.play();
    }

    @FXML
    private void actioneight(ActionEvent event) {
        bar8Note.play();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // TODO
    }

}
